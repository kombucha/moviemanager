import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;


public class FailFrame extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	private JPanel cards;
	private JPanel controlPanel;
	private JLabel posLabel;
	private JButton nextButton;
	private JButton previousButton;
	private JButton endButton;
	private int index;
	
	private ArrayList<String> movieList;
	
	public FailFrame(ArrayList<Movie> list)
	{
		this.movieList = new ArrayList<String>();
		this.index = 0;
		
		this.cards = new JPanel(new CardLayout());
		initGlassPane();
		
		for(Movie m : list)
		{
			cards.add(new MoviePanel(this, m), m.getFileName());
			movieList.add(m.getFileName());
		}
		CardLayout cl = (CardLayout)(cards.getLayout());
	    cl.show(cards, this.movieList.get(index));
		
		this.controlPanel = new JPanel();
		this.nextButton = new JButton("Next");
		this.nextButton.addActionListener(this);
		this.previousButton = new JButton("Previous");
		this.previousButton.addActionListener(this);
		this.previousButton.setEnabled(false);
		this.endButton = new JButton("Finish");
		this.endButton.addActionListener(this);
		this.posLabel = new JLabel((index+1) +"/" + this.movieList.size());
		
		this.controlPanel.add(this.previousButton);
		this.controlPanel.add(this.posLabel);
		this.controlPanel.add(this.nextButton);
		this.controlPanel.add(this.endButton);
		
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(this.cards, BorderLayout.CENTER);
		this.getContentPane().add(this.controlPanel, BorderLayout.SOUTH);
		
		this.setVisible(true);
		this.requestFocus();
		this.setTitle("Failed search");
		this.setMinimumSize(new Dimension(805,980));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void initGlassPane()
	{
		JPanel glass = new JPanel()
		{
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g)
		    {
		        Graphics2D g2 = (Graphics2D) g;
		        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
		    }
		};
		
		glass.addMouseListener(new MouseAdapter() {});
	    glass.addMouseMotionListener(new MouseMotionAdapter() {});
	    glass.addKeyListener(new KeyAdapter() {});

		glass.setOpaque(false);
		glass.setLayout(new BorderLayout());
		
		JProgressBar pb = new JProgressBar();
		pb.setIndeterminate(true);
		pb.setFont(new Font("SansSerif", Font.PLAIN,30));
		pb.setStringPainted(true);
		pb.setString("Work Pending");
		glass.add(pb, BorderLayout.CENTER);	
		this.setGlassPane(glass);		
	}

	
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() instanceof JButton)
		{
			JButton b = (JButton)event.getSource();
			
			if(b.equals(this.nextButton))
			{
				index++;
				
				
				if(index < this.movieList.size() - 1)
				{
					this.nextButton.setEnabled(true);
				    CardLayout cl = (CardLayout)(cards.getLayout());
				    cl.show(cards, this.movieList.get(index));
				}
				else if(index == this.movieList.size() - 1)
				{
					this.nextButton.setEnabled(false);
				    CardLayout cl = (CardLayout)(cards.getLayout());
				    cl.show(cards, this.movieList.get(index));	
				}
				else
				{
					this.nextButton.setEnabled(false);
					this.index = this.movieList.size() - 1;
				}
				
				if(index > 0)
					this.previousButton.setEnabled(true);
				
				this.posLabel.setText((index+1) +"/" + this.movieList.size());
			}			
			else if(b.equals(this.previousButton))
			{
				index--;
				
				if(index > 0)
				{
					this.previousButton.setEnabled(true);
				    CardLayout cl = (CardLayout)(cards.getLayout());
				    cl.show(cards, this.movieList.get(index));
				}
				else if(index == 0)
				{
					this.previousButton.setEnabled(false);
				    CardLayout cl = (CardLayout)(cards.getLayout());
				    cl.show(cards, this.movieList.get(index));
				}
				else
				{
					this.previousButton.setEnabled(false);
					this.index = 0;
				}
				
				if(index < this.movieList.size() - 1)
					this.nextButton.setEnabled(true);
				
				this.posLabel.setText((index+1) +"/" + this.movieList.size());
			}
			else if(b.equals(this.endButton))
			{
				this.dispose();
			}
			
		}
		
	}
}
