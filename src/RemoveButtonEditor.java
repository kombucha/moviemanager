import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

 
public class RemoveButtonEditor extends AbstractCellEditor implements ActionListener, TableCellEditor
{
	private static final long serialVersionUID = 1L;
	
	private JButton button;
    private JTable table; 
    
    public RemoveButtonEditor(JTable t)
    {        
        button = new JButton("-");
        table = t;
        button.addActionListener(this);
        button.setBorderPainted(false); 
    }
    
    
    public void actionPerformed(ActionEvent e)
    {
    	int id = new Integer(this.table/*.getModel()*/.getValueAt(this.table.getSelectedRow(), 0).toString());
    	if(JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this movie?", "Confimation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
    	{
    		MainMoviesManager.myController.removeMovie(id);
    		MainMoviesManager.myMainFrame.updateUIData();
    	}
        
    }
 
    //Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column)
    {
        return this.button;
    }

	public Object getCellEditorValue()
	{
		System.out.println("Oh hai");
		return this.button;
	}
}
