import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;


public class MyMouseListener implements MouseListener
{

	@Override
	public void mouseClicked(MouseEvent mc)
	{
		if(mc.getSource() instanceof JTable)
		{
			JTable table = (JTable) mc.getSource();

			if(table.getSelectedRow() >=0 && table.getSelectedColumn() >= 0)
			{
				//+1 pour la colonne car j'ai retir� la colonne id de la vue mais pas du mod�le, d'o� le d�calage
				Object value = table/*.getModel()*/.getValueAt(table.getSelectedRow(), table.getSelectedColumn()/* + 1*/);
				int id = new Integer(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
				System.out.println(id);
				
				if(value instanceof Boolean)
				{
					MainMoviesManager.myController.updateSeen(id, (Boolean) value);
				}
			}
		}
		

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

}
