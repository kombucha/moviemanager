
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Movie
{
	private int id;
	
	private String title;
	private String rate;
	private String votes;
	private String year;	
	private String director;
	private String plot;
	private String runtime;
	private String absolutePath;
	private String fileName;
	private String size;
	private String release;
	private String type;
	private String imdb;
	private String seen;
	
	private ArrayList<String> genre;
	private ArrayList<String> actors;
	private ArrayList<String> characters;
	private ArrayList<String> trivia;
	private ArrayList<String> goof;
	
	public Movie()
	{
		this.id 		  =   -1;
		this.title        =   "Unknown";
		this.rate         =   "0";
		this.votes        =   "0";
		this.year         =   "0";
		this.director     =   "Unknown";
		this.plot         =   "Unknown";
		this.runtime      =   "0";
		this.absolutePath =   "Unknown";
		this.fileName     =   "Unknown";
		this.size         =   "0";
		this.release      =   "Unknown";
		this.type		  =   "Unknown";
		this.imdb		  =   "Unknown";
		this.seen         =   "0";
		
		this.genre        =   new ArrayList<String>();
		this.actors       =   new ArrayList<String>();
		this.characters   =   new ArrayList<String>();
		this.trivia       =   new ArrayList<String>();
		this.goof         =   new ArrayList<String>();
	}	

	public String getTitle() {return title;}
	public String getRate() {return rate;}
	public String getVotes() {return votes;}
	public String getDirector() {return director;}
	public String getYear() {return year;}
	public String getAbsolutePath() {return absolutePath;}
	public String getFileName() {return fileName;}
	public String getRuntime() {return runtime;}
	public int getId() {return id;}
	public String getSize() {return size;}
	public String getRelease() {return release;}
	public String getType() {return type;}
	public String getPlot() {return plot;}
	public String getImdb() {return imdb;}
	public String getSeen() {return seen;}


	public void setTitle(String title)
	{
		if(title.length() > Controller.LIMIT_TITLE)
			this.title = title.substring(0, Controller.LIMIT_TITLE - 10) + "*too long*";
		else
			this.title = title;
	}
	
	public void setDirector(String director)
	{
		if(director.length() > Controller.LIMIT_DIRECTORS)
			this.director = director.substring(0, Controller.LIMIT_DIRECTORS - 10) + "*too long*";
		else
			this.director = director;
	}
	
	public void setAbsolutePath(String absolutePath)
	{
		if(absolutePath.length() > Controller.LIMIT_ABSOLUTEPATH)
			this.absolutePath = absolutePath.substring(0, Controller.LIMIT_ABSOLUTEPATH - 10) + "*too long*";
		else
			this.absolutePath = absolutePath;
	}
	
	public void setFileName(String fileName)
	{
		if(fileName.length() > Controller.LIMIT_FILENAME)
			this.fileName = fileName.substring(0, Controller.LIMIT_FILENAME - 10) + "*too long*";
		else
			this.fileName = fileName;
	}
	
	public void setRelease(String release)
	{
		if(release.length() > Controller.LIMIT_RELEASE)
			this.release = release.substring(0, Controller.LIMIT_RELEASE - 10) + "*too long*";
		else
			this.release = release;
	}
	
	public void setPlot(String plot)
	{
		if(plot.length() > Controller.LIMIT_PLOT)
			this.plot = plot.substring(0, Controller.LIMIT_PLOT - 10) + "*too long*";
		else
			this.plot = plot;
	}
	
	public void setType(String type)
	{
		if(type.length() > Controller.LIMIT_TYPE)
			this.type = type.substring(0, Controller.LIMIT_TYPE - 10) + "*too long*";
		else
			this.type = type;
	}
	
	public void setImdb(String imdb)
	{
		if(imdb.length() > Controller.LIMIT_IMDB)
			this.imdb = imdb.substring(0, Controller.LIMIT_IMDB - 10) + "*too long*";
		else
			this.imdb = imdb;
	}
	
	public void setSeen(String seen)
	{
		if(seen.equals("0") || seen.equals("1"))
			this.seen = seen;
	}
	
	public void setRate(String rate) {this.rate = rate;}
	public void setVotes(String votes) {this.votes = votes;}
	public void setYear(String year) {this.year = year;}
	public void setRuntime(String runtime) {this.runtime = runtime;}
	public void setId(int id) {this.id = id;}
	public void setSize(String size) {this.size = size;}

	
	
	public ArrayList<String> getGenre() {return genre;}
	public ArrayList<String> getTrivia() {return trivia;}
	public ArrayList<String> getGoof() {return goof;}
	public ArrayList<String> getActors() {return actors;}
	public ArrayList<String> getCharacters() {return characters;}	

	public void setGenre(ArrayList<String> genre) {this.genre = genre;}
	public void setActors(ArrayList<String> actors) {this.actors = actors;}
	public void setCharacters(ArrayList<String> characters) {this.characters = characters;}
	public void setTrivia(ArrayList<String> trivia) {this.trivia = trivia;}
	public void setGoof(ArrayList<String> goof) {this.goof = goof;}

	public void addCastInfo(String a, String c)
	{
		if(a.length() > Controller.LIMIT_ACTOR)
			this.actors.add(a.substring(0, Controller.LIMIT_ACTOR - 10) + "*too long*");
		else
			this.actors.add(a);
		
		if(c.length() > Controller.LIMIT_CHARACTER)
			this.characters.add(c.substring(0, Controller.LIMIT_CHARACTER - 10) + "*too long*");	
		else
			this.characters.add(c);		
	}
	
	public void addGenre(String g)
	{
		if(g.length() > Controller.LIMIT_GENRE)
			this.genre.add(g.substring(0, Controller.LIMIT_GENRE - 10) + "*too long*");
		else
			this.genre.add(g);
		
	}
	
	public void addTrivia(String t)
	{
		if(t.length() > Controller.LIMIT_TRIVIA)
			this.trivia.add(t.substring(0, Controller.LIMIT_TRIVIA - 10) + "*too long*");
		else
			this.trivia.add(t);
			
	}
	
	public void addGoof(String g)
	{
		if(g.length() > Controller.LIMIT_GOOF)
			this.goof.add(g.substring(0, Controller.LIMIT_GOOF - 10) + "*too long*");
		else
			this.goof.add(g);			
	}
	
	
	public boolean saveMovie()
	{	
		try
		{
			int i = 0;
			
			Connection c = MainMoviesManager.myController.getConnection();
			
			if(c == null)
				return false;
			
			Statement s = c.createStatement();
			PreparedStatement ps = null;
			
			//On v�rifie que le film n'existe pas d�j�
			ResultSet rs = s.executeQuery("SELECT file_name FROM movie;");
			
			while(rs.next())
				if(rs.getObject("file_name").toString().equals(this.fileName))
				{
					System.out.println("Movie already exists ! (" + this.title + ")");
					
					rs.close();
					s.close();
					c.close();					
					return false;
				}
			
			//Updating movie table
			try
			{
	        	ps = c.prepareStatement("INSERT INTO movie VALUES(NEXTVAL('movie_seq'), ?, " + this.year + ", ?, ?, " + this.runtime + ", " + this.rate + ", " + this.votes + ", ?, ?, " + this.size + ", ?, ?, 0, ?);");
	        	ps.setString(1, this.title);
	        	ps.setString(2, this.director);
	        	ps.setString(3, this.plot);
	        	ps.setString(4, this.fileName);
	        	ps.setString(5, this.absolutePath);
	        	ps.setString(6, this.release);
	        	ps.setString(7, this.type);
	        	ps.setString(8, this.imdb);
	        	ps.executeUpdate();
			}
        	catch(Exception e){e.printStackTrace();}//*/
	        
	        //Updating genre table
	        for(String str : this.genre)
	        {
				try
				{
		        	ps = c.prepareStatement("INSERT INTO genre VALUES (CURRVAL('movie_seq'), ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e){e.printStackTrace();}
	        }//*/
	        
	        
	      //Updating cast table
	        for(i = 0; i < this.actors.size(); i++)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO cast VALUES (CURRVAL('movie_seq'), ?, ?);");
		        	ps.setString(1, this.actors.get(i));
		        	ps.setString(2, this.characters.get(i));
		        	ps.executeUpdate();
				}
        		catch(Exception e){e.printStackTrace();}
	        }//*/
	        
	        //Updating trivia table
	        for(String str : this.trivia)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO trivia VALUES (CURRVAL('movie_seq'), ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e){e.printStackTrace();}
	        	
	        }//*/
	        
	        //Updating goofs table
	        for(String str : this.goof)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO goofs VALUES (CURRVAL('movie_seq'), ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e){e.printStackTrace();}
	        }//*/
	        
	        //Gettin id
	        rs = s.executeQuery("SELECT INFORMATION_SCHEMA.SEQUENCES.CURRENT_VALUE FROM INFORMATION_SCHEMA.SEQUENCES WHERE INFORMATION_SCHEMA.SEQUENCES.SEQUENCE_NAME = 'MOVIE_SEQ';");
	        if(rs.next())
	        	this.id = new Integer(rs.getObject(1).toString());
	        
			
	        rs.close();
			ps.close();
			s.close();
			c.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean loadMovie(int id)
	{
		this.id = id;
		
		try
		{
			Connection c = MainMoviesManager.myController.getConnection();
			if(c == null)
				return false;
			
			Statement s = c.createStatement();
			
			ResultSet res = s.executeQuery("SELECT * FROM movie WHERE id_movie = " + id + ";");
			
			while(res.next())
			{
				this.id = res.getInt("id_movie");
				this.title = res.getString("title");
				this.rate = res.getString("rate");
				this.votes = res.getString("votes");
				this.year = res.getString("year");	
				this.director = res.getString("directors");;
				this.runtime = res.getString("runtime");;
				this.absolutePath = res.getString("absolute_path");;
				this.fileName = res.getString("file_name");;
				this.size = res.getString("size");;
				this.release = res.getString("release");;
				this.type = res.getString("type");
				this.plot = res.getString("plot");
				this.imdb = res.getString("imdb");
				this.seen = res.getString("seen");//*/
			}
			
			
			res = s.executeQuery("SELECT * FROM genre WHERE genre_id_movie = " + id + ";");
			while(res.next())
				this.addGenre(res.getString("genre_name"));	
			
			res = s.executeQuery("SELECT * FROM trivia WHERE trivia_id_movie = " + id + ";");
			while(res.next())
				this.addTrivia(res.getString("trivia_txt"));	
			
			res = s.executeQuery("SELECT * FROM goofs WHERE goofs_id_movie = " + id + ";");
			while(res.next())
				this.addGoof(res.getString("goof_txt"));
			
			
			res = s.executeQuery("SELECT * FROM cast WHERE cast_id_movie = " + id + ";");
			while(res.next())
			{
				this.addCastInfo(res.getString("actor"), res.getString("character"));
			}
			
			
			s.close();
			c.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean updateMovie(int id)
	{
		this.id = id;
		System.out.println("Updating movie");
		
		try
		{
			int i = 0;
			
			Connection c = MainMoviesManager.myController.getConnection();
			
			if(c == null)
				return false;
			
			PreparedStatement ps = null;
			
			
			//Updating movie table
			try
			{
	        	ps = c.prepareStatement("UPDATE movie SET title = ?, year = " + this.year + ", directors = ?, plot = ?, runtime = " + this.runtime + ", rate = " + this.rate + ", votes = " + this.votes + ", file_name = ?, absolute_path = ?, size = " + this.size + ", release = ?, type = ?, imdb = ?, seen = " + this.seen + " WHERE id_movie =" + this.id + ";");
	        	
	        	ps.setString(1, this.title);
	        	ps.setString(2, this.director);
	        	ps.setString(3, this.plot);
	        	ps.setString(4, this.fileName);
	        	ps.setString(5, this.absolutePath);
	        	ps.setString(6, this.release);
	        	ps.setString(7, this.type);
	        	ps.setString(8, this.imdb);
	        	ps.executeUpdate();
			}
        	catch(Exception e)
        	{
        		System.out.println("Failed to update movie line");
        		e.printStackTrace();
        	}//*/
	        
	        //Updating genre table
        	try
        	{
	        	ps = c.prepareStatement("DELETE FROM genre WHERE genre_id_movie = " + this.id + ";");
	        	ps.executeUpdate();
        	}
        	catch(Exception e)
        	{
        		System.out.println("Failed to delete genre");
        		e.printStackTrace();
        	}
        	
	        for(String str : this.genre)
	        {
				try
				{
					ps = c.prepareStatement("INSERT INTO genre VALUES (" + this.id + ", ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e)
	        	{
	        		System.out.println("Failed to update genre");
	        		e.printStackTrace();
	        	}
	        }//*/
	        
	        
	      //Updating cast table
        	try
        	{
	        	ps = c.prepareStatement("DELETE FROM cast WHERE cast_id_movie = " + this.id + ";");
	        	ps.executeUpdate();
        	}
        	catch(Exception e)
        	{
        		System.out.println("Failed to delete cast");
        		e.printStackTrace();
        	}
        	
	        for(i = 0; i < this.actors.size(); i++)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO cast VALUES (" + this.id + ", ?, ?);");
		        	ps.setString(1, this.actors.get(i));
		        	ps.setString(2, this.characters.get(i));
		        	ps.executeUpdate();
				}
        		catch(Exception e)
        		{
        			System.out.println("Failed to update cast");
        			e.printStackTrace();
        		}
	        }//*/
	        
	        //Updating trivia table
        	try
        	{
	        	ps = c.prepareStatement("DELETE FROM trivia WHERE trivia_id_movie = " + this.id + ";");
	        	ps.executeUpdate();
        	}
        	catch(Exception e)
        	{
        		System.out.println("Failed to delete trivia");
        		e.printStackTrace();
        	}
	        for(String str : this.trivia)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO trivia VALUES (" + this.id + ", ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e)
	        	{
	        		System.out.println("Failed to update trivia");
	        		e.printStackTrace();
	        	}
	        	
	        }//*/
	        
	        //Updating goofs table
        	try
        	{
	        	ps = c.prepareStatement("DELETE FROM goofs WHERE goofs_id_movie = " + this.id + ";");
	        	ps.executeUpdate();
        	}
        	catch(Exception e)
        	{
        		System.out.println("Failed to delete goofs");
        		e.printStackTrace();
        	}
        	
	        for(String str : this.goof)
	        {
	        	try
	        	{
		        	ps = c.prepareStatement("INSERT INTO goofs VALUES (" + this.id + ", ?);");
		        	ps.setString(1, str);
		        	ps.executeUpdate();
				}
	        	catch(Exception e)
	        	{
	        		System.out.println("Failed to update goofs");
	        		e.printStackTrace();
	        	}
	        }//*/
	        
			ps.close();
			c.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public String toString()
	{
		StringBuffer str = new StringBuffer();
		int i = 0;
		
		str.append("Titre : " + this.title + "\n");
		str.append("Year : " + this.year + "\n");
		str.append("Director(s) : " + this.director + "\n");
		str.append("Rate : " + this.rate + "/10\n");
		str.append("Votes : " + this.votes + "\n");
		
		str.append("Genres : ");
		for(String s : this.genre)
			str.append(s + " ");
		str.append("\n");
		
		str.append("Runtime : " + this.runtime + "\n");
		
		str.append("\n");
		str.append("Cast : \n");		
		for(i = 0; i < this.actors.size(); i++)
			str.append(this.actors.get(i) + "\tas\t" + this.characters.get(i)+"\n");
		str.append("\n");
		
		str.append("\n");
		str.append("Trivia : \n");
		for(String s : this.trivia)
			str.append(s + "\n");
		str.append("\n");
		
		str.append("\n");
		str.append("Goofs : \n");
		for(String s : this.goof)
			str.append(s + "\n");
		str.append("\n");
		
		return str.toString();
	}
	
}
