import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class MainMoviesManager
{
	public static Controller myController;
	public static MainFrame myMainFrame;
	
	public static void main(String[] args)
	{
		myController = new Controller();
		
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){};
			
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				myMainFrame = new MainFrame(myController);				
			}
			
		});
		
	}
}
