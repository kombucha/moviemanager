import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class MovieDataRetriever
{
	private String moviePageURL;
	private String searchedMovie;
	private URLReader myURLReader;
	
	private ArrayList<String> moviePage;
	private ArrayList<String> plotPage;
	private ArrayList<String> castPage;
	private ArrayList<String> triviaPage;
	private ArrayList<String> goofsPage;
	
	public MovieDataRetriever(String str, boolean mode) throws Exception
	{	
		//System.out.println("Starting search at " + new Date(System.currentTimeMillis()));
		
		this.myURLReader = new URLReader();
		
		if(mode)
		{
			this.searchedMovie = parseTitle(str);
			this.moviePageURL = getMovieURL();
			if(this.moviePageURL == null)
				throw new Exception();
		}
		else
			this.moviePageURL = str;
		
		//System.out.println("Movie URL : " + this.moviePageURL);
		
		this.moviePage  = getMoviePage();
		this.plotPage   = getPlotPage();
		this.castPage   = getCastPage();
		this.triviaPage = getTriviaPage();
		this.goofsPage  = getGoofsPage();
		
		//System.out.println("Ending search at " + new Date(System.currentTimeMillis()));
	}
	
	private String getMovieURL()
	{
		try
		{
			//this.myURLReader.setURL("http://www.imdb.com/find?s=tt&q=" + this.searchedMovie.replaceAll(" ", "+"));
			this.myURLReader.setURL("http://www.google.fr/search?&q=site:imdb.com+" + this.searchedMovie.replaceAll(" ", "+") + "&btnI=745");
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			return null;
		}
		ArrayList<String> list = this.myURLReader.getPage();
		
		int i = 0, j = 0;
		String tempLine = "";
		Pattern myPattern = Pattern.compile(".*?<a href=\"(/title/.*?/)\".*");
		Pattern myPattern_b = Pattern.compile(".*?<link rel=\"canonical\" href=\"http://www.imdb.com(/title/tt.*?/).*");
		
		for(i = 0; i < list.size(); i++)
		{
			tempLine = list.get(i);
			
			if(tempLine.startsWith("<title>") && !tempLine.contains("IMDb Title Search"))
			{
				Matcher myMatcher_b;
				
				for(j = 0; j < list.size(); j++)
				{
					myMatcher_b = myPattern_b.matcher(list.get(j));
					if(myMatcher_b.matches())
						return "http://www.imdb.com" + myMatcher_b.group(1);
				}
				
			}
			else if(tempLine.startsWith(" <p><b>Popular Titles</b>"))
			{
				Matcher myMatcher = myPattern.matcher(tempLine);
				
				if(myMatcher.matches())
					return "http://www.imdb.com" + myMatcher.group(1);					
				else
					System.out.println("Doesn't match :(");
			}
			
		}
		
		return null;
	}
	
	private ArrayList<String> getMoviePage()
	{
		try
		{
			this.myURLReader.setURL(this.moviePageURL);
		}
		catch (MalformedURLException e) {e.printStackTrace();}
		return myURLReader.getPage();
	}
	
	private ArrayList<String> getPlotPage()
	{
		try
		{
			this.myURLReader.setURL(this.moviePageURL + "plotsummary");
		}
		catch (MalformedURLException e) {e.printStackTrace();}
		return this.myURLReader.getPage();
	}
	
	private ArrayList<String> getCastPage()
	{
		try
		{
			this.myURLReader.setURL(this.moviePageURL + "fullcredits#cast");
		}
		catch (MalformedURLException e) {e.printStackTrace();}		
		return this.myURLReader.getPage();
	}
	
	private ArrayList<String> getTriviaPage()
	{
		try
		{
			this.myURLReader.setURL(this.moviePageURL + "trivia");
		}
		catch (MalformedURLException e) {e.printStackTrace();}	
		return this.myURLReader.getPage();
	}
	
	private ArrayList<String> getGoofsPage()
	{
		try
		{
			this.myURLReader.setURL(this.moviePageURL + "goofs");
		}
		catch (MalformedURLException e) {e.printStackTrace();}	
		return this.myURLReader.getPage();
	}
	
	public String getMovieTitle()
	{
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			Pattern title = Pattern.compile(".*?<title>(.*?)\\(\\d{4}.*?\\).*?</title>.*");
			Matcher m_title;
			
			for(String str : this.moviePage)
			{
				m_title = title.matcher(str);
				
				if(m_title.matches())
					return cleanString(m_title.group(1));
			}
		}
		
		return "Unknown Title";
	}
	
	public String getMovieYear()
	{
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			Pattern year = Pattern.compile(".*?<title>.*?\\((\\d{4}).*?\\).*?</title>.*");
			Matcher m_year;
			
			for(String str : this.moviePage)
			{
				m_year = year.matcher(str);
				
				if(m_year.matches())
					return cleanString(m_year.group(1));
			}
		}
		
		return "0";
	}
	
	public String getMovieRate()
	{
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			Pattern rate = Pattern.compile(".*?(\\d\\.\\d)/10</b>.*");
			Matcher m_rate;
			
			for(String str : this.moviePage)
			{
				m_rate = rate.matcher(str);
				
				if(m_rate.matches())
					return cleanString(m_rate.group(1));
			}
		}
		
		return "0";
	}
	
	public String getMovieVotes()
	{
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			Pattern votes = Pattern.compile(".*?class=\"tn15more\">(.*?) votes</a>.*");
			Matcher m_votes;
			
			for(String str : this.moviePage)
			{
				m_votes = votes.matcher(str);
				
				if(m_votes.matches())
					return cleanString(m_votes.group(1).replaceAll(",", ""));
			}
		}
		
		return "0";
	}
	
	public ArrayList<String> getMovieGenres()
	{
		ArrayList<String> genresList = new ArrayList<String>();
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			int i = 0, j = 0;
			Pattern genres = Pattern.compile(".*?<a href=\"/Sections/Genres/.*?\">(.*?)</a>.*");
			Matcher m_genres;
			
			for(i = 0; i < this.moviePage.size(); i++)
			{
				if(this.moviePage.get(i).equals("<h5>Genre:</h5>"))
				{
					for(j = i; j < this.moviePage.size(); j++)
					{
						m_genres = genres.matcher(this.moviePage.get(j));
						
						if(m_genres.matches())
						{	
							for(String str : this.moviePage.get(j).split(" \\| "))
							{
								m_genres = genres.matcher(str);
								if(m_genres.matches())
								{
									genresList.add(cleanString(m_genres.group(1)));
								}
							}
							break;							
						}
					}					
					break;
				}
			}
		}
		
		return genresList;
	}

	public String getMovieRuntime()
	{
		if(this.moviePage != null && this.moviePage.size() > 1)
		{
			int i = 0, j = 0;
			for(i = 0; i < this.moviePage.size(); i++)
			{
				if(this.moviePage.get(i).matches("\\s*<h5>Runtime:</h5>\\s*"))
				{
					for(j = i; j < this.moviePage.size(); j++)
					{
						if(this.moviePage.get(j).contains("min"))
						{
							String runtime = cleanString(this.moviePage.get(j));
							Integer run = 1000;
							
							for(String s : runtime.split("[^\\d+]"))
							{
								if( !s.equals("") && new Integer(s) < run)
									run = new Integer(s);//*/						
							}
							return run.toString();
						}
					}
					break;
				}
			}
		}
		
		return "0";
	}

	public ArrayList<String> getMovieCast()
	{
		ArrayList<String> castList = new ArrayList<String>();
		try
		{
			if(this.castPage != null && this.castPage.size() > 1)
			{
				int i = 0;
				String temp = "";
				
				Pattern castLine = Pattern.compile(".*?<table class=\"cast\">.*");
				
				Matcher m_castLine;
				
				for(i = 0; i < this.castPage.size(); i++)
				{
					temp = this.castPage.get(i);
					m_castLine = castLine.matcher(temp);
					
					if(m_castLine.matches())
					{
						int j = 0;
						
						temp = temp.replaceAll("<td.*?>", "#").replaceAll("<.*?>", " ").replaceAll(" +", " ").replaceAll(" ?# ... # ?", "#").replaceAll(" ?# # ", "|");					
						temp = temp.substring(((temp.indexOf("Cast (in credits order)") == -1)? 0:temp.indexOf("Cast (in credits order)")), (temp.indexOf(" Create a character page for: ") == -1)? temp.length():temp.indexOf(" Create a character page for: "));
						temp = temp.replaceFirst(".*?\\|", "");
						
						temp = cleanString(temp);
						String[] splitCast = temp.split("\\|");
						String[] splitActChar;
						for(j = 0; j < splitCast.length; j++)
						{		
							splitActChar = splitCast[j].split("\\#");
							if(splitActChar.length > 2 || splitActChar.length == 1)
								continue;
							castList.add(splitActChar[0] + "#|#" + splitActChar[1]);					
						}
					}
				}
			}
		}
		catch(Exception e) {e.printStackTrace();}
		
		return castList;
	}
	
	public String getMovieDirector()
	{
		try
		{
			if(this.castPage != null && this.castPage.size() > 1)
			{
				int i = 0;
				String temp = "";
				
				Pattern castLine = Pattern.compile(".*?<table class=\"cast\">.*");			
				Matcher m_castLine;
				
				for(i = 0; i < this.castPage.size(); i++)
				{
					temp = this.castPage.get(i);
					m_castLine = castLine.matcher(temp);
					
					if(m_castLine.matches())
					{
						String director;
						
						temp = cleanString(temp);
						temp = temp.replaceAll("<td.*?>", "#").replaceAll("<.*?>", " ").replaceAll(" +", " ").replaceAll(" ?# ... # ?", "#").replaceAll(" ?# # ", "|");
						
						director = temp.substring(0, (temp.indexOf("|# Writing credits") == -1)? temp.indexOf("Writing credits"):temp.indexOf("|# Writing credits"));
						director = director.replaceAll(" \\#\\|", ", ").replaceAll("[^\\w \\(\\),]", "").replace("Directed by ", "").replaceAll(" +", " ");
						
						return cleanString(director);
					}
				}
			}
		}
		catch(Exception e){}
		
		return "Unknown Director";
	}
	
	public ArrayList<String> getMovieTrivia()
	{
		ArrayList<String> triviaList = new ArrayList<String>();
		
		if(this.triviaPage != null && this.triviaPage.size() > 1)
		{
			int i = 0;
			String temp = "";
			
			Pattern triviaLine = Pattern.compile(".*?<li><a name=.*?></a> .*");
			Matcher m_triviaLine;
			
			for(i = 0; i < this.triviaPage.size(); i++)
			{
				temp = this.triviaPage.get(i);
				m_triviaLine = triviaLine.matcher(temp);
				
				if(m_triviaLine.matches())
				{
					temp = temp.replaceAll("<.*?>", "");
					
					triviaList.add(cleanString(temp));
				}				
			}
		}		
		return triviaList;
	}
	
	public ArrayList<String> getMovieGoofs()
	{
		ArrayList<String> goofsList = new ArrayList<String>();
		
		if(this.goofsPage != null && this.goofsPage.size() > 1)
		{
			int i = 0;
			String temp = "";
		
			Pattern goofLine = Pattern.compile(".*?<li><a name=.*?></a>.*");		
			Matcher m_goofLine;

			for(i = 0; i < this.goofsPage.size(); i++)
			{
				temp = this.goofsPage.get(i);
				m_goofLine = goofLine.matcher(temp);
				
				if(m_goofLine.matches())
				{
					temp = temp.replaceAll("<.*?>", "");
					
					goofsList.add(cleanString(temp));
				}
				
			}
		}		
		return goofsList;
	}
	
	public String getMoviePlot()
	{
		StringBuffer moviePlot = new StringBuffer();
		String temp = "";
		if(this.plotPage != null && this.plotPage.size() > 1)
		{
			int i = 0;
			Pattern votes = Pattern.compile(".*?<p class=\"plotpar\">.*");
			Matcher m_votes;
			
			for(i = 0; i < this.plotPage.size(); i++)
			{
				m_votes = votes.matcher(this.plotPage.get(i));
				
				if(m_votes.matches())
				{
					temp = this.plotPage.get(i+1);
					temp = cleanString(temp.replaceAll("<.*?>", "").replaceAll("\\n", ""));
					moviePlot.append(temp + "\n");					
				}
			}
		}
		
		return moviePlot.toString();		
	}
	
	public String getImdb()
	{
		return this.moviePageURL;
	}
	
	private String parseTitle(String str)
	{
		return str.substring(0, str.lastIndexOf('.')).replaceAll(".*\\\\", "").replaceAll(" \\[.*?\\]-.*", "").replaceAll("\\[.*?\\]", "");
	}
	
	private String cleanString(String str)
	{
		int i = 0;
		
		//Cleaning html special character
		synchronized(HTMLCode.array)
		{
			for(i = 0; i < HTMLCode.array.length; i++)
				str = str.replaceAll(HTMLCode.array[i][0], HTMLCode.array[i][1]);
		}
		
		String[] split = str.replaceAll("&#.*?;", "").split("\\s+");
		StringBuffer res = new StringBuffer();		
		
		for(i = 0; i < split.length; i++)
		{
			if(!split[i].equals(""))
				if(i != split.length - 1)
					res.append(split[i] + " ");
				else
					res.append(split[i]);
		}
			
		return res.toString();
	}
}
