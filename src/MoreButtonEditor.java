import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

 
public class MoreButtonEditor extends AbstractCellEditor implements ActionListener, TableCellEditor
{
	private static final long serialVersionUID = 1L;
	
	private JButton button;
    private JTable table; 
    
    public MoreButtonEditor(JTable t)
    {        
        this.button = new JButton("More");
        table = t;
        this.button.addActionListener(this);
        this.button.setBorderPainted(false); 
    }
    
    
    public synchronized void actionPerformed(ActionEvent e)
    {
    	SwingUtilities.invokeLater(new Runnable()
    	{
			public void run()
			{
				JFrame movieFrame = new JFrame();
		    	MoviePanel moviePanel = new MoviePanel(movieFrame, new Integer(table/*.getModel()*/.getValueAt(table.getSelectedRow(), 0).toString()));
		    	movieFrame.setContentPane(moviePanel);
		    	
		    	//Initialisation glasspane
				JPanel glass = new JPanel()
				{
					private static final long serialVersionUID = 1L;

					public void paintComponent(Graphics g)
				    {
				        Graphics2D g2 = (Graphics2D) g;
				        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
				    }
				};
				
				glass.addMouseListener(new MouseAdapter() {});
			    glass.addMouseMotionListener(new MouseMotionAdapter() {});
			    glass.addKeyListener(new KeyAdapter() {});

				glass.setOpaque(false);
				glass.setLayout(new BorderLayout());
				
				JProgressBar pb = new JProgressBar();
				pb.setIndeterminate(true);
				pb.setFont(new Font("SansSerif", Font.PLAIN,30));
				pb.setStringPainted(true);
				pb.setString("Work Pending");
				glass.add(pb, BorderLayout.CENTER);	
				movieFrame.setGlassPane(glass);
				//Initialisation glasspane
		    	
		    	movieFrame.setTitle(moviePanel.getMyMovie().getTitle());
		    	movieFrame.setMinimumSize(new Dimension(805,930));
		    	movieFrame.setVisible(true);
		    	movieFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				
			}
    		
    	});
    }
 
    //Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column)
    {
        return this.button;
    }

	public Object getCellEditorValue()
	{
		System.out.println("Oh hai");
		return this.button;
	}
}
