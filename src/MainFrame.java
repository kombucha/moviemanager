import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableColumnModel;


public class MainFrame extends JFrame implements ActionListener
{
	/**
	 * Pour enlever le warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Controller controller;
	private JPanel containerPanel;
	private JPanel mainPanel;
	private JPanel searchFormPanel;
	private JPanel resultPanel;
	private JPanel statusPanel;
	private JTable resultTable;
	private JScrollPane resultScrollPane;
	
	private JLabel titleLabel;
	private JTextField titleField;
	
	private JLabel directorLabel;
	private JTextField directorField;
	
	private JLabel actorLabel;
	private JTextField actorField;
	
	private JLabel runtimeLabel;
	private JButton runtimeButton;
	private JComboBox runtimeComboBox;
	
	private JLabel yearLabel1;
	private JLabel yearLabel2;
	private JComboBox yearComboBox1;
	private JComboBox yearComboBox2;
	
	private JLabel genreLabel;
	private JCheckBox genreCheckBox;
	private JList genreList;
	private DefaultListModel genreListModel;
	private JScrollPane genreScrollPane;
	
	private JLabel rateLabel;
	private JButton rateButton;
	private JComboBox rateComboBox;
	
	private JComboBox seenComboBox;
	private JLabel seenLabel;
	
	private JButton clearFieldsButton;
	private JButton addFilesButton;
	private JButton queryButton;
	private JCheckBox onlyAvailableCheckBox;

	private JLabel statusLabel;
	public static JProgressBar searchProgressBar;	
	
	public MainFrame(Controller cm)
	{
		this.controller = MainMoviesManager.myController;
		
		this.setTitle("Movies Manager");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setSize(800, 600);
		
		this.containerPanel = new JPanel();
		this.setContentPane(this.containerPanel);
		
		initSearchForm();
		this.resultPanel = new JPanel();
		this.mainPanel = new JPanel();
		this.statusPanel = new JPanel();
		
		
		String[] titles = {"ID", "Title", "Year", "Runtime", "Rate", "Seen?", "More Info", "Remove"};
		MyTableModel myModel = new MyTableModel(this.controller.getMainTableDatas(null, false), titles);
		this.resultTable = new JTable();
		//Va falloir choisir fiston, soit j'affiche l'id et j'peux trier sans crainte, soit je l'affiche pas et je trie pas
		this.resultTable.setAutoCreateRowSorter(true);
		
		this.resultTable.setColumnModel(new DefaultTableColumnModel()
		{
			private static final long serialVersionUID = 1L;

			public void moveColumn(int columnIndex, int newIndex)
			{  
				if (columnIndex == 0 || newIndex == 0)
					return; // don't allow  
				super.moveColumn(columnIndex, newIndex);  
			}  
		
		});//*/
		this.resultTable.setModel(myModel);
		this.resultTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.resultTable.getColumnModel().getColumn(5).setCellRenderer(this.resultTable.getDefaultRenderer(Boolean.class));
		this.resultTable.getColumnModel().getColumn(5).setCellEditor(this.resultTable.getDefaultEditor(Boolean.class));		
		this.resultTable.addMouseListener(new MyMouseListener());
		
		
		this.resultTable.getColumnModel().getColumn(6).setCellRenderer(new MoreButtonRenderer());
		this.resultTable.getColumnModel().getColumn(6).setCellEditor(new MoreButtonEditor(this.resultTable));		
		this.resultTable.getColumnModel().getColumn(7).setCellRenderer(new RemoveButtonRenderer());
		this.resultTable.getColumnModel().getColumn(7).setCellEditor(new RemoveButtonEditor(this.resultTable));


		this.resultTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		this.resultTable.getColumnModel().getColumn(1).setPreferredWidth(200);
		this.resultTable.getColumnModel().getColumn(5).setPreferredWidth(15);
		this.resultTable.getColumnModel().getColumn(6).setPreferredWidth(25);
		this.resultTable.getColumnModel().getColumn(6).setPreferredWidth(40);
		this.resultTable.getColumnModel().getColumn(7).setPreferredWidth(25);
		
		//this.resultTable.removeColumn(this.resultTable.getColumn("ID"));
		
		//Gestion du drag'n'drop
		this.resultTable.setDragEnabled(true);
		TransferHandler myTransferHandler = new TransferHandler()
		{
			private static final long serialVersionUID = 1L;

			public boolean canImport(TransferHandler.TransferSupport info)
			{
                // we only import Strings
                if (!info.isDataFlavorSupported(DataFlavor.javaFileListFlavor) || MainMoviesManager.myController.workInProgress)
                	return false;
                else
                	return true;                
            }

            public boolean importData(TransferHandler.TransferSupport info)
            {
            	System.out.println("import data function 1");
                if (!info.isDrop())
                    return false;

                // Check for file flavor
                if (!info.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
                {
                    System.out.println("List doesn't accept a drop of this type.");
                    return false;
                }

                // Get the file that is being dropped.
                Transferable t = info.getTransferable();
                ArrayList<File> temp = new ArrayList<File>();
                
                try
                {
                	int i = 0;
                	
                	for(i = 0; i < ((List) t.getTransferData(DataFlavor.javaFileListFlavor)).size(); i++)
                		temp.add(((List<File>) t.getTransferData(DataFlavor.javaFileListFlavor)).get(i));
                	
                	final  ArrayList<File> chosenFiles = (ArrayList<File>) temp.clone();               	
                	
					Thread myThread = new Thread(new Runnable()
														{
															public void run()
															{
																controller.addMovies(chosenFiles);																
															}
															
														});		
					myThread.start();
					
					System.out.println("Adding data by drag'n'drop, amaaazing!");
                } 
                catch (Exception e)
                {
                	e.printStackTrace();
                	return false;
                }

				return false;
            }
            
            public int getSourceActions(JComponent c)
            {
                return COPY;
            }
		};
		this.resultTable.setTransferHandler(myTransferHandler);//*/

		this.resultScrollPane = new JScrollPane(this.resultTable);
		
		this.statusPanel.setLayout(new BorderLayout());
		
		statusLabel = new JLabel("  " + ((MyTableModel)this.resultTable.getModel()).getRowCount() + " movies selected  ");
		
		searchProgressBar = new JProgressBar();
		searchProgressBar.setPreferredSize(new Dimension(this.getWidth(), 20));
		searchProgressBar.setEnabled(false);
		searchProgressBar.setVisible(false);
							
		this.containerPanel.setLayout(new BorderLayout());
		this.mainPanel.setLayout(new GridLayout(0,1));
		this.resultScrollPane.setBorder(BorderFactory.createTitledBorder("Result : "));
		
		this.statusPanel.add(statusLabel, BorderLayout.WEST);
		this.statusPanel.add(searchProgressBar, BorderLayout.CENTER);
		
		this.resultPanel.add(this.resultScrollPane);			
		this.containerPanel.add(this.mainPanel, BorderLayout.CENTER);		
		this.containerPanel.add(this.statusPanel, BorderLayout.SOUTH);
		this.mainPanel.add(this.searchFormPanel);
		this.mainPanel.add(this.resultScrollPane);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void initSearchForm()
	{
		int i = 0, j = 0;
		
		this.searchFormPanel = new JPanel();
		this.searchFormPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,5,5);
		this.searchFormPanel.setBorder(BorderFactory.createTitledBorder("Search Form : "));
	
		
		this.titleLabel = new JLabel("Title : ");
		this.titleField = new JTextField();
		this.titleField.setPreferredSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.titleLabel, c);
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.titleField, c);
		
		
		this.directorLabel = new JLabel("Director : ");
		this.directorField = new JTextField();
		this.directorField.setPreferredSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.directorLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.directorField, c);
		
		this.actorLabel = new JLabel("Actor : ");
		this.actorField = new JTextField();
		this.actorField.setPreferredSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.actorLabel, c);
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.actorField, c);
		
		//Separator
		c.gridx = 2;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 3;
		c.weightx = 0;
		c.weighty = 0;
		this.searchFormPanel.add(new JSeparator(JSeparator.VERTICAL), c);	
		this.searchFormPanel.add(new JSeparator(JSeparator.VERTICAL), c);
		this.searchFormPanel.add(new JSeparator(JSeparator.VERTICAL), c);
		
		this.runtimeLabel = new JLabel("Runtime ");
		this.runtimeButton = new JButton("<=");
		this.runtimeButton.addActionListener(this);
		this.runtimeComboBox = new JComboBox();
		this.runtimeComboBox.setPreferredSize(new Dimension(60,20));
		
		for(i = 0; i <= 300; i+=5)
			this.runtimeComboBox.addItem(i);
		
		c.gridx = 3;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.runtimeLabel, c);	
		c.gridx = 4;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.runtimeButton, c);
		c.gridx = 6;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.runtimeComboBox, c);

		
		this.rateLabel = new JLabel("Rate ");
		this.rateButton = new JButton(">=");
		this.rateButton.addActionListener(this);
		this.rateComboBox = new JComboBox();
		this.rateComboBox.setPreferredSize(new Dimension(60,20));
		
		for(i = 0; i <= 10; i++)
			this.rateComboBox.addItem(i);

		
		c.gridx = 3;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.rateLabel, c);	
		c.gridx = 4;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.rateButton, c);
		c.gridx = 6;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.rateComboBox, c);
		
		this.yearLabel1 = new JLabel("Years between ");
		this.yearLabel2 = new JLabel(" and ");
		this.yearComboBox1 = new JComboBox();
		this.yearComboBox2 = new JComboBox();
		this.yearComboBox1.setPreferredSize(new Dimension(60,20));
		this.yearComboBox2.setPreferredSize(new Dimension(60,20));
		
		for(j = 19; j <= 20; j++)
			for(i = 0; i < 10; i++)
			{
				this.yearComboBox1.addItem(j + "" + i + "0");
				this.yearComboBox2.addItem(j + "" + i + "0");
			}

		
		
		c.gridx = 3;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.yearLabel1, c);	
		c.gridx = 4;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.yearComboBox1, c);
		c.gridx = 5;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.yearLabel2, c);
		c.gridx = 6;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.yearComboBox2, c);

		
		this.genreLabel = new JLabel("Genre : ");
		this.genreCheckBox = new JCheckBox("AND", true);
		this.genreListModel = new DefaultListModel();
		
		for(String s: this.controller.getGenres())
			this.genreListModel.addElement(s);
		
		this.genreList = new JList(this.genreListModel);
		this.genreScrollPane = new JScrollPane(this.genreList);
		this.genreScrollPane.setPreferredSize(new Dimension(90,120));
		
		c.gridx = 7;
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.genreLabel, c);
		c.gridx = 7;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridheight = 4;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.genreScrollPane, c);
		c.gridx = 7;
		c.gridy = 5;
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.genreCheckBox, c);
		
		
		this.seenLabel = new JLabel("Include : ");
		this.seenComboBox = new JComboBox();
		this.seenComboBox.addItem("Seen");
		this.seenComboBox.addItem("Not Seen");
		this.seenComboBox.addItem("Both");
		this.seenComboBox.setSelectedItem("Both");
		
		c.gridx=3;
		c.gridy=4;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		this.searchFormPanel.add(this.seenLabel, c);
		c.gridx=4;
		c.gridy=4;
		this.searchFormPanel.add(this.seenComboBox, c);
		
		
		this.addFilesButton = new JButton("Add files...");
		this.addFilesButton.addActionListener(this);
		this.clearFieldsButton = new JButton("Clear fields");
		this.clearFieldsButton.addActionListener(this);
		this.queryButton = new JButton("Launch Query");
		this.queryButton.addActionListener(this);
		this.onlyAvailableCheckBox = new JCheckBox("Only available movies?");
		c.gridx = 0;
		c.gridy = 8;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 0;
		c.weighty = 0;		
		this.searchFormPanel.add(this.addFilesButton, c);
		c.gridx = 1;
		c.gridy = 8;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.clearFieldsButton, c);
		c.gridx = 3;
		c.gridy = 8;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 0;		
		this.searchFormPanel.add(this.queryButton, c);
		c.gridwidth = 2;
		c.gridheight = 1;
		c.gridx = 4;
		c.gridy = 8;		
		this.searchFormPanel.add(this.onlyAvailableCheckBox, c);
		c.gridwidth = 1;
		c.gridheight = 1;
		
		
		clearFields();		
	}

	public synchronized void updateUIData()
	{
		Thread myThread = new Thread(new Runnable()
											{
												public void run()
												{
													final Object[][] datas = controller.getMainTableDatas(createConditions(), onlyAvailableCheckBox.isSelected());
													final ArrayList<String> genres = controller.getGenres();
													
													SwingUtilities.invokeLater(new Runnable()
																					{
																						public void run()
																						{
																							System.out.println("I'm updating");
																							//Update de la table de resultat
																							((MyTableModel) resultTable.getModel()).setDatas(datas);
																							resultTable.setSelectionModel(new DefaultListSelectionModel());
																							resultTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
																							
																							//Update de la liste des genres
																							int[] selectedIndices = genreList.getSelectedIndices();
																							
																							genreListModel.removeAllElements();
																							for(String s: genres)
																								genreListModel.addElement(s);
																							
																							genreList.setSelectedIndices(selectedIndices);
																							
																							statusLabel.setText("  " + ((MyTableModel)resultTable.getModel()).getRowCount() + " movies selected  ");
																						}
																						
																					});
													
												}
												
											});
		myThread.start();	
	}
	
	public synchronized void setProgressBar(final int max)
	{
		SwingUtilities.invokeLater(new Runnable()
		{				
			public void run()
			{
				searchProgressBar.setMinimum(0);
				searchProgressBar.setMaximum(max);
				searchProgressBar.setString("0%");
				searchProgressBar.setStringPainted(true);
				searchProgressBar.setEnabled(true);
				searchProgressBar.setVisible(true);
			}					
		});
	
	}
	
	public synchronized void updateProgressBar()
	{
		synchronized(searchProgressBar)
		{	
			synchronized(this.statusLabel)
			{
				SwingUtilities.invokeLater(new Runnable()
				{
					public void run()
					{
						searchProgressBar.setValue(searchProgressBar.getValue() + 1);
						searchProgressBar.setString((int)((float)searchProgressBar.getValue()/(float)searchProgressBar.getMaximum() * 100) + "%");
						statusLabel.setText("  " + searchProgressBar.getValue() + "/" + MainFrame.searchProgressBar.getMaximum() + "  ");
						
						if(searchProgressBar.getValue() == searchProgressBar.getMaximum())
						{						
							searchProgressBar.setValue(0);
							searchProgressBar.setStringPainted(false);
							searchProgressBar.setEnabled(false);
							searchProgressBar.setVisible(false);
						}
						
					}
					
				});
			}
		}
	}
	
	private void clearFields()
	{
		this.titleField.setText("");
		this.directorField.setText("");
		this.actorField.setText("");
		this.runtimeComboBox.setSelectedItem(300);
		this.runtimeButton.setText("<=");
		this.yearComboBox1.setSelectedItem("1900");
		this.yearComboBox2.setSelectedItem("2010");
		this.genreList.setSelectedIndices(new int[0]);
		this.genreCheckBox.setSelected(true);
		this.rateComboBox.setSelectedItem(0);
		this.rateButton.setText(">=");
		this.seenComboBox.setSelectedItem("Both");
		this.onlyAvailableCheckBox.setSelected(false);
	}
	
	private ArrayList<String> createConditions()
	{
		ArrayList<String> conditionList = new ArrayList<String>();
		
		//title condition
		if(!this.titleField.getText().equals(""))
			conditionList.add("LOWER(movie.title) LIKE '" + likePattern(this.titleField.getText()) + "'");
		
		//director condition
		if(!this.directorField.getText().equals(""))
			conditionList.add("LOWER(movie.directors) LIKE '" + likePattern(this.directorField.getText()) + "'");
		
		//actor condition
		if(!this.actorField.getText().equals(""))
			conditionList.add("id_movie IN (SELECT cast_id_movie FROM cast WHERE LOWER(cast.actor) LIKE '" + likePattern(this.actorField.getText()) + "')");
		
		//runtime condition
		conditionList.add("movie.runtime " + this.runtimeButton.getText() + " " + this.runtimeComboBox.getSelectedItem().toString());
		
		//genre condition
		if(this.genreList.getSelectedIndex() != -1)
		{
			String genres = "";
			Object[] oGenres = this.genreList.getSelectedValues();
			String[] sGenres = new String[this.genreList.getSelectedValues().length];			
			for(int i = 0; i < sGenres.length; i++)
				sGenres[i] = oGenres[i].toString();//*/			
			
			if(!this.genreCheckBox.isSelected())
			{
				System.out.println("Hai");
				genres = "(";
				
				for(String str : sGenres)
					if(!str.equals("*"))
						genres += "'" + str + "',";
				genres = genres.substring(0, genres.length() - 1);
				genres += ")";
				
				conditionList.add("id_movie IN (SELECT genre_id_movie FROM genre WHERE genre_name IN " + genres + ")");
			}
			else
				for(String str : sGenres)
					if(!str.equals("*"))
						conditionList.add("(SELECT COUNT( * ) FROM genre WHERE genre.genre_id_movie = movie.id_movie AND genre_name LIKE '" + str + "') > 0");//*/
		}
		
		//year condition
		int year_min = (new Integer(this.yearComboBox1.getSelectedItem().toString()) < new Integer(this.yearComboBox2.getSelectedItem().toString()))? new Integer(this.yearComboBox1.getSelectedItem().toString()):new Integer(this.yearComboBox2.getSelectedItem().toString());
		int year_max = (new Integer(this.yearComboBox1.getSelectedItem().toString()) > new Integer(this.yearComboBox2.getSelectedItem().toString()))? new Integer(this.yearComboBox1.getSelectedItem().toString()):new Integer(this.yearComboBox2.getSelectedItem().toString());
		
		conditionList.add("(movie.year BETWEEN " + year_min + " AND " + year_max + " OR movie.year=0)");
		
		//rate condition
		conditionList.add("movie.rate " + this.rateButton.getText() + " " + this.rateComboBox.getSelectedItem().toString());
		
		//Seen condition
		if(this.seenComboBox.getSelectedItem().equals("Seen"))
			conditionList.add("seen = 1");
		else if (this.seenComboBox.getSelectedItem().equals("Not Seen"))
			conditionList.add("seen = 0");
		
		return conditionList;
	}
	
	private String likePattern(String str)
	{
		String res = str;
		
		res = res.replaceAll(" ", "%").replaceAll("-", "%").replaceAll("'", "%");
		res = "%" + res + "%";
		res = res.toLowerCase();
		
		return res;
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource() instanceof JButton)
		{			
			JButton testB = (JButton) ae.getSource();			

			if(testB.getText().equals("Add files..."))
			{
				if(!MainMoviesManager.myController.workInProgress)
				{
					JFileChooser chooseFiles = new JFileChooser();
					
					chooseFiles.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					chooseFiles.showOpenDialog(this);
					File chosenFile = chooseFiles.getSelectedFile();
	
					
					if(chosenFile != null)
					{
						final String chosenFileName = chosenFile.toString();
						Thread myThread = new Thread(new Runnable()
															{
																public void run()
																{
																	System.out.println(chosenFileName);
																	controller.addMovies(chosenFileName);																
																}
																
															});		
						myThread.start();
					}
					chooseFiles = null;
				}
				else
					JOptionPane.showMessageDialog(null, "Wait till the first search is over", "Warning", JOptionPane.WARNING_MESSAGE);
					
			}
			//Bouton clearFields
			else if(testB.getText().equals("Clear fields"))
			{
				clearFields();
			}
			//Bouton query
			else if(testB.getText().equals("Launch Query"))
			{
				createConditions();
				updateUIData();
			}
			else if(testB.equals(this.runtimeButton))
			{
				if(testB.getText().equals("<="))
					testB.setText(">=");
				else
					testB.setText("<=");
			}
			else if(testB.equals(this.rateButton))
			{
				if(testB.getText().equals("<="))
					testB.setText(">=");
				else
					testB.setText("<=");
			}			
			else
				System.out.println("Default");
		}
	}


}
