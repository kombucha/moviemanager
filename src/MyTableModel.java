import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;


public class MyTableModel extends DefaultTableModel
{
	private static final long serialVersionUID = 1L;

	public MyTableModel(Object[][] datas, String[] titles)
	{
		super();
		this.setColumnIdentifiers(titles);
		this.setDatas(datas);		
	}
	
	public boolean isCellEditable(int row, int col)
	{
		return getValueAt(0,col) instanceof Boolean || getValueAt(0,col) instanceof JButton || getValueAt(0,col) == null;
	}

	public void setDatas(Object[][] datas)
	{
		int i = 0;
		
		while(this.getRowCount() > 0)
			this.removeRow(0);
		
		if(datas == null)
			return;
		
		for(i = 0; i < datas.length; i++)
			this.addRow(datas[i]);
		
		this.fireTableDataChanged();
		
	}

	
}
