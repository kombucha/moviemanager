import java.util.LinkedList;

public class WorkQueue
{
	//private final int nThreads;
	private final PoolWorker[] threads;
	private final LinkedList<Runnable> queue;
	
	public WorkQueue(int nThreads)
	{
		//this.nThreads = nThreads;
		queue = new LinkedList<Runnable>();
		threads = new PoolWorker[nThreads];
		for (int i=0; i < nThreads; i++)
		{
			threads[i] = new PoolWorker();
			threads[i].start();
		}
	}
	
	public void execute(Runnable r)
	{
		synchronized(queue)
		{
			queue.addLast(r);
			queue.notify();
		}
	}
	
	//Surement inutile...
	public void finishThreads()
	{
		synchronized(queue)
		{
			for(PoolWorker pw : threads)
				pw.setKeepOnGroovin(false);
			
			queue.notifyAll();
		}
		
		System.out.println("Finishing Threads!");
	}

	private class PoolWorker extends Thread
	{
		private boolean keepOnGroovin = true;
		
		public void run()
		{
			Runnable r;
			
			while (keepOnGroovin)
			{
				synchronized(queue)
				{
					while (queue.isEmpty() && keepOnGroovin)
					{
						try
						{
							queue.wait();
						}
						catch (InterruptedException ignored) {}
					}
					
					if(!keepOnGroovin) break;
					
					r = (Runnable) queue.removeFirst();
				}
				// If we don't catch RuntimeException, 
				// the pool could leak threads
				try
				{
					r.run();
				}
				catch (RuntimeException e) {e.printStackTrace();}
				
			}
		}
		
		public synchronized void setKeepOnGroovin(boolean b) {this.keepOnGroovin = b;}
	}
}
