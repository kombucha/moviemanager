
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class URLReader 
{
	final private static int TENTATIVE = 5;
	
	private String inputLine;
	private URL myURL;
	private URLConnection myConn;
	private BufferedReader in; 
	private ArrayList<String> page;
	
	public URLReader()
	{
		this.myURL = null;
		this.inputLine = "";
		this.myConn = null;
		this.in = null;
	}

	public void setURL(String str) throws MalformedURLException {this.myURL = new URL(str);}
	
	public ArrayList<String> getPage()
	{
		int countdown = TENTATIVE;
		this.page = new ArrayList<String>();
		
		if(this.myURL != null)
		{
			InputStreamReader isr = null;
			while(countdown-- > 0)
			{
				try
				{
					this.myConn = (HttpURLConnection)this.myURL.openConnection();
					this.myConn.setConnectTimeout(2000);
					this.myConn.setRequestProperty("User-agent","Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
			
					isr = new InputStreamReader(myConn.getInputStream());
					in = new BufferedReader(isr);
					
					while ((inputLine = in.readLine()) != null)
					{
						this.page.add(inputLine);
					}
					countdown = 0;	
					//System.out.println(this.myURL + " succesfully acquired");
				}
				catch(IOException e)
				{
					//System.out.println("Error dealing with " + this.myURL);
				}
				finally
				{
					try
					{						
						isr.close();
						in.close();
					}
					catch (IOException e) {}					
				}
				
			}
		}
		
		return page;
	}

	public void showContent()
	{
		if(this.page != null && this.page.size() > 1)
		{
			for(int i = 0; i < page.size(); i++)
			{
				System.out.println(page.get(i));
			}
		}
		else
			System.out.println("Page is empty");
	}
}
