import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class MoviePanel extends JPanel implements ActionListener, MouseListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Movie myMovie;
	private int id;
	
	private JLabel ImdbLabel;
	private JTextField imdbField;
	private JButton changeInfoButton;
	private JButton imdbOpenButton;
	
	private JLabel titleLabel;
	private JTextField titleField;
	
	private JLabel directorLabel;
	private JTextField directorField;
	
	private JLabel yearLabel;
	private JTextField yearField;
	
	private JLabel runtimeLabel;
	private JTextField runtimeField;
	
	private JLabel rateLabel;
	private JTextField rateField;
	
	private JLabel votesLabel;
	private JTextField votesField;
	
	private JLabel genresLabel;
	private JTextField genresField;

	private JLabel releaseLabel;
	private JTextField releaseField;
	
	private JLabel typeLabel;
	private JTextField typeField;
	
	private JCheckBox seenCheckBox;
	
	private JLabel plotLabel;
	private JTextArea plotTextArea; 
	private JScrollPane plotScrollPane;
	
	private JLabel castLabel;
	private JTable castTable;
	private MyTableModel castModel;
	private JScrollPane castScrollPane;
	private JTextField actorField;
	private JTextField characterField;
	private JButton castAddButton;
	private JButton castRemButton;
	
	private JLabel triviaLabel;
	private JTextArea triviaTextArea;
	private JScrollPane triviaScrollPane;
	private JTextField triviaField;
	private JButton triviaAddButton;
	private JButton triviaRemButton;	
	
	private JLabel goofLabel;
	private JTextArea goofTextArea;
	private JScrollPane goofScrollPane;
	private JTextField goofField;
	private JButton goofAddButton;
	private JButton goofRemButton;
	
	private JLabel fileNameLabel;
	private JLabel fileDirectoryLabel;
	private JLabel fileSizeLabel;
	private JLabel filePresenceLabel;
	
	private JButton saveButton;
	private JButton playButton;
	private JButton changeFileButton;
	
	private JFrame myFrame;
	
	public MoviePanel()
	{
		this.setBorder(BorderFactory.createTitledBorder("Movie Info : "));
		this.setLayout(new GridBagLayout());
		
		initComponents();
		this.id = -1;
		this.myMovie = new Movie();
	}
	
	public MoviePanel(JFrame myFrame, int id)
	{
		this.myFrame = myFrame;
		this.setBorder(BorderFactory.createTitledBorder("Movie Info : "));
		this.setLayout(new GridBagLayout());

		initComponents();
		this.id = id;
		this.myMovie = new Movie();
		this.myMovie.loadMovie(this.id);
		loadInfos();
	}
	
	public MoviePanel(JFrame myFrame, Movie m)
	{
		this.myFrame = myFrame;
		this.setBorder(BorderFactory.createTitledBorder("Movie Info : "));
		this.setLayout(new GridBagLayout());

		initComponents();
		this.id = -1;
		this.myMovie = m;

		loadInfos();
	}

	public Movie getMyMovie() {return myMovie;}
	public void setMyMovie(Movie myMovie) {this.myMovie = myMovie;}
	
	//Initialisation des �l�ments du panel et placement
	private void initComponents()
	{	
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 1;
		c.insets = new Insets(5,5,5,5);
		
		this.ImdbLabel = new JLabel("IMDB Link : ");
		this.imdbField = new JTextField();
		this.imdbField.setMinimumSize(new Dimension(150,20));
		this.changeInfoButton = new JButton("Change Infos");
		this.changeInfoButton.addActionListener(this);
		this.imdbOpenButton = new JButton("Open Link");
		this.imdbOpenButton.addActionListener(this);
		
		c.gridx = 1;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		this.add(this.ImdbLabel, c);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 2;
		this.add(this.imdbField, c);
		c.gridwidth = 1;
		c.gridx = 4;
		c.gridy = 0;
		this.add(this.changeInfoButton, c);
		c.gridwidth = 2;
		c.gridx = 5;
		c.gridy = 0;
		this.add(this.imdbOpenButton, c);
		c.gridwidth = 1;		
		

		this.titleLabel = new JLabel("Title : ");
		this.titleField = new JTextField();
		this.titleField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 2;
		this.add(this.titleLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		this.add(this.titleField, c);
		

		this.directorLabel = new JLabel("Director(s) : ");
		this.directorField = new JTextField();
		this.directorField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 3;
		this.add(this.directorLabel, c);
		c.gridx = 1;
		c.gridy = 3;
		this.add(this.directorField, c);
		
		
		this.yearLabel = new JLabel("Year : ");
		this.yearField = new JTextField();
		this.yearField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 4;
		this.add(this.yearLabel, c);
		c.gridx = 1;
		c.gridy = 4;
		this.add(this.yearField, c);
		
		
		this.runtimeLabel = new JLabel("Runtime : ");
		this.runtimeField = new JTextField();
		this.runtimeField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 5;
		this.add(this.runtimeLabel, c);
		c.gridx = 1;
		c.gridy = 5;
		this.add(this.runtimeField, c);
		
		
		this.rateLabel = new JLabel("Rate : ");
		this.rateField = new JTextField();
		this.rateField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 8;
		this.add(this.rateLabel, c);
		c.gridx = 1;
		c.gridy = 8;
		this.add(this.rateField, c);
		
		
		this.votesLabel = new JLabel("Votes : ");
		this.votesField = new JTextField();
		this.votesField.setMinimumSize(new Dimension(200,20));
		
		c.gridx = 0;
		c.gridy = 9;
		this.add(this.votesLabel, c);
		c.gridx = 1;
		c.gridy = 9;
		this.add(this.votesField, c);
		
		this.genresLabel = new JLabel("Genres : ");
		this.genresField = new JTextField();
		this.genresField.setMinimumSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 10;
		this.add(this.genresLabel, c);
		c.gridx = 1;
		c.gridy = 10;
		this.add(this.genresField, c);
		
		this.releaseLabel = new JLabel("Release : ");
		this.releaseField = new JTextField();
		this.releaseField.setMinimumSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 11;
		this.add(this.releaseLabel, c);
		c.gridx = 1;
		c.gridy = 11;
		this.add(this.releaseField, c);
		
		this.typeLabel = new JLabel("Type : ");
		this.typeField = new JTextField();
		this.typeField.setMinimumSize(new Dimension(200,20));
		c.gridx = 0;
		c.gridy = 12;
		this.add(this.typeLabel, c);
		c.gridx = 1;
		c.gridy = 12;
		this.add(this.typeField, c);
		
		this.seenCheckBox = new JCheckBox("Seen?");
		c.gridx = 0;
		c.gridy = 13;
		this.add(this.seenCheckBox, c);
		
		this.castLabel = new JLabel("Cast : ");
		String[] castTitles = {"Actor", "Character"};
		Object[][] defaultCast = {{"default", "default"}};
		this.castModel = new MyTableModel(defaultCast, castTitles);
		this.castTable = new JTable(this.castModel);
		this.castTable.setAutoCreateRowSorter(true);
		this.castTable.addMouseListener(this);
		this.castScrollPane = new JScrollPane(this.castTable);
		this.castScrollPane.setMinimumSize(new Dimension(400,150));
		this.actorField = new JTextField();
		this.actorField.setMinimumSize(new Dimension(150,20));
		this.characterField = new JTextField();
		this.characterField.setMinimumSize(new Dimension(150,20));
		this.castAddButton = new JButton("+");
		this.castAddButton.addActionListener(this);
		this.castRemButton = new JButton("-");
		this.castRemButton.addActionListener(this);
		
		
		c.gridx = 3;
		c.gridy = 1;
		this.add(this.castLabel, c);
		c.gridx = 3;
		c.gridy = 2;
		c.gridwidth = 4;
		c.gridheight = 4;
		this.add(this.castScrollPane, c);
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 6;
		this.add(this.actorField, c);
		c.gridx = 4;
		c.gridy = 6;
		this.add(this.characterField, c);
		c.gridx = 5;
		c.gridy = 6;
		this.add(this.castAddButton, c);
		c.gridx = 6;
		c.gridy = 6;
		this.add(this.castRemButton, c);
		
		
		this.triviaLabel = new JLabel("Trivia : ");
		this.triviaTextArea = new JTextArea();
		this.triviaTextArea.setEditable(false);
		this.triviaTextArea.setLineWrap(true);
		this.triviaTextArea.setWrapStyleWord(true);
		this.triviaScrollPane = new JScrollPane(this.triviaTextArea);
		this.triviaScrollPane.setMinimumSize(new Dimension(400,200));
		
		this.triviaField = new JTextField();
		this.triviaField.setMinimumSize(new Dimension(300, 20));
		this.triviaAddButton = new JButton("+");
		this.triviaAddButton.addActionListener(this);
		this.triviaRemButton = new JButton("-");
		this.triviaRemButton.addActionListener(this);
		
		c.gridx = 3;
		c.gridy = 7;
		this.add(this.triviaLabel, c);
		c.gridx = 3;
		c.gridy = 8;
		c.gridheight = 5;
		c.gridwidth = 4;
		this.add(this.triviaScrollPane, c);
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 13;
		c.gridwidth = 2;
		this.add(this.triviaField, c);
		c.gridwidth = 1;
		c.gridx = 5;
		c.gridy = 13;		
		this.add(this.triviaAddButton, c);		
		c.gridx = 6;
		c.gridy = 13;
		this.add(this.triviaRemButton, c);
		
		this.plotLabel = new JLabel("Plot : ");
		this.plotTextArea = new JTextArea();
		this.plotTextArea.setLineWrap(true);
		this.plotTextArea.setWrapStyleWord(true);
		this.plotScrollPane = new JScrollPane(this.plotTextArea);
		this.plotScrollPane.setMinimumSize(new Dimension(300,200));
		c.gridx = 0;
		c.gridy = 14;		
		this.add(this.plotLabel, c);		
		c.gridx = 0;
		c.gridy = 15;
		c.gridwidth = 2;
		this.add(this.plotScrollPane, c);
		c.gridwidth = 1;
		
		
		this.goofLabel = new JLabel("Goof : ");
		this.goofTextArea = new JTextArea();
		this.goofTextArea.setEditable(false);
		this.goofTextArea.setLineWrap(true);
		this.goofTextArea.setWrapStyleWord(true);
		this.goofScrollPane = new JScrollPane(this.goofTextArea);
		this.goofField = new JTextField();
		this.goofField.setMinimumSize(new Dimension(300, 20));
		
		this.goofAddButton = new JButton("+");
		this.goofRemButton = new JButton("-");
		this.goofAddButton.addActionListener(this);
		this.goofRemButton.addActionListener(this);
		this.goofScrollPane.setMinimumSize(new Dimension(400,200));
		
		c.gridx = 3;
		c.gridy = 14;
		this.add(this.goofLabel, c);
		c.gridx = 3;
		c.gridy = 15;
		c.gridwidth = 4;
		this.add(this.goofScrollPane, c);
		c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 16;
		c.gridwidth = 2;
		this.add(this.goofField, c);
		c.gridwidth = 1;
		c.gridx = 5;
		c.gridy = 16;		
		this.add(this.goofAddButton, c);
		c.gridx = 6;
		c.gridy = 16;		
		this.add(this.goofRemButton, c);
		
		
		
		this.fileNameLabel = new JLabel("test");
		this.fileSizeLabel = new JLabel();
		this.fileDirectoryLabel = new JLabel();
		this.filePresenceLabel = new JLabel();
		
		c.gridx = 0;
		c.gridy = 18;
		c.gridwidth = 6;
		this.add(this.fileNameLabel, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 19;
		this.add(this.fileSizeLabel, c);
		c.gridx = 0;
		c.gridy = 20;
		c.gridwidth = 6;
		this.add(this.fileDirectoryLabel, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 21;
		this.add(this.filePresenceLabel, c);
		c.gridwidth = 1;//*/
		
		this.playButton = new JButton("Play");
		this.playButton.setEnabled(false);
		this.playButton.addActionListener(this);
		this.changeFileButton = new JButton("Change file");
		this.changeFileButton.addActionListener(this);
		c.gridx = 0;
		c.gridy = 22;
		this.add(this.playButton, c);
		c.gridx = 1;
		c.gridy = 22;
		this.add(this.changeFileButton, c);
		
		this.saveButton = new JButton("Save");
		this.saveButton.addActionListener(this);
		c.gridx = 5;
		c.gridy = 22;
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.LAST_LINE_END;
		this.add(this.saveButton, c);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridwidth = 1;
	}

	//Charge les infos dans la fiche
	private void loadInfos()
	{
		int i = 0;
		
		this.imdbField.setText(this.myMovie.getImdb());
		this.titleField.setText(this.myMovie.getTitle());
		this.directorField.setText(this.myMovie.getDirector());
		this.yearField.setText(this.myMovie.getYear());
		this.runtimeField.setText(this.myMovie.getRuntime());
		this.rateField.setText(this.myMovie.getRate());
		this.votesField.setText(this.myMovie.getVotes());	
		this.releaseField.setText(this.myMovie.getRelease());
		this.typeField.setText(this.myMovie.getType());
		
		if(this.myMovie.getSeen().equals("0"))
			this.seenCheckBox.setSelected(false);
		else
			this.seenCheckBox.setSelected(true);
		
		this.plotTextArea.setText(this.myMovie.getPlot());
		this.plotTextArea.setCaretPosition(0);
		
		String genres = "";
		for(String s : this.myMovie.getGenre())
			genres += s + " ";
		this.genresField.setText(genres);
		
		StringBuffer triviaTxt = new StringBuffer();
		for(i = 0; i < this.myMovie.getTrivia().size(); i++)
		{						
			triviaTxt.append(i + ". " +this.myMovie.getTrivia().get(i));
			triviaTxt.append("\n___________________________________________\n");
		}
		this.triviaTextArea.setText(triviaTxt.toString());
		this.triviaTextArea.setCaretPosition(0);
		
		
		StringBuffer goofTxt = new StringBuffer();
		for(i = 0; i < this.myMovie.getGoof().size(); i++)
		{			
			goofTxt.append(i + ". " + this.myMovie.getGoof().get(i));
			goofTxt.append("\n___________________________________________\n");
		}
		this.goofTextArea.setText(goofTxt.toString());
		this.goofTextArea.setCaretPosition(0);
		
		i = 0;
		Object[][] oCast = new Object[this.myMovie.getActors().size()][2];
		for(i = 0; i < this.myMovie.getActors().size(); i++)
		{
			oCast[i][0] = this.myMovie.getActors().get(i);
			oCast[i][1] = this.myMovie.getCharacters().get(i);
		}
		((MyTableModel) this.castTable.getModel()).setDatas(oCast);
		
		
		this.fileNameLabel.setText("Filename : " + this.myMovie.getFileName());
		this.fileSizeLabel.setText("Size : ~" + new Long(new Long(this.myMovie.getSize())/1000000) + " Mo");
		this.fileDirectoryLabel.setText("Path : " + this.myMovie.getAbsolutePath());
		

		File testFile = new File(this.myMovie.getAbsolutePath() + this.myMovie.getFileName());		
		if(testFile.exists())
		{
			this.filePresenceLabel.setText("File OK!");
			this.playButton.setEnabled(true);
		}
		else
		{
			this.filePresenceLabel.setText("Can't find the file...");
			this.playButton.setEnabled(false);
		}
	}

	//R�cupere les donn�s de la fiche et les enregistre
	private void changeInfos()
	{
		System.out.println("Getting changes...");
		
		this.myMovie.setTitle(this.titleField.getText());
		this.myMovie.setDirector(this.directorField.getText());
		this.myMovie.setRelease(this.releaseField.getText());
		this.myMovie.setType(this.typeField.getText());
		this.myMovie.setImdb(this.imdbField.getText());
		this.myMovie.setPlot(this.plotTextArea.getText());
		
		try
		{
			Integer testYear = new Integer(this.yearField.getText());			
			if(testYear > 1900 && testYear < 2100)
				this.myMovie.setYear(this.yearField.getText());			
		}
		catch(Exception e){};
		
		try
		{
			Integer testRuntime = new Integer(this.runtimeField.getText());			
			if(testRuntime >= 0 && testRuntime <= 300)
				this.myMovie.setRuntime(this.runtimeField.getText());			
		}
		catch(Exception e){};
		
		try
		{
			Float testRate = new Float(this.rateField.getText());			
			if(testRate >= 0 && testRate <= 10)
				this.myMovie.setRate(this.rateField.getText());			
		}
		catch(Exception e){};
		
		try
		{
			Integer testVotes = new Integer(this.votesField.getText());			
			if(testVotes >= 0)
				this.myMovie.setVotes(this.votesField.getText());			
		}
		catch(Exception e){};
		
		if(this.seenCheckBox.isSelected())
			this.myMovie.setSeen("1");
		else
			this.myMovie.setSeen("0");
		
		
		ArrayList<String> genresList = new ArrayList<String>();
		String[] splitG = this.genresField.getText().split(" ");
		for(String str : splitG)
		{
			genresList.add(str);
		}
		this.myMovie.setGenre(genresList);
	}
	
	//Action Listener
	public void actionPerformed(ActionEvent event)
	{		
		if(event.getSource() instanceof JButton)
		{
			//Action pour le bouton de sauvegarde
			if(event.getSource().equals(this.saveButton))
			{
				changeInfos();
				if(this.id != -1)
					this.myMovie.updateMovie(this.id);
				else
				{
					this.myMovie.saveMovie();
					this.id = this.myMovie.getId();
				}
				
				this.loadInfos();
				MainMoviesManager.myMainFrame.updateUIData();
			}
			//Action pour le bouton d'ajout de trivia
			else if(event.getSource().equals(this.triviaAddButton) && !this.triviaField.getText().equals(""))
			{
				this.myMovie.addTrivia(this.triviaField.getText());
				loadInfos();
				this.triviaField.setText("");//*/
			}
			//Action pour le bouton d'enl�vement de trivia
			else if(event.getSource().equals(this.triviaRemButton))
			{
				try
				{
					int testedIndex = new Integer(this.triviaField.getText());
					
					if(testedIndex > 0 && testedIndex < this.myMovie.getTrivia().size())
					{
						this.myMovie.getTrivia().remove(testedIndex);
					}
				}
				catch(Exception e){}//*/
				loadInfos();
				this.triviaField.setText("");
			}
			//Action pour le bouton d'ajout de goof
			else if(event.getSource().equals(this.goofAddButton) && !this.goofField.getText().equals(""))
			{
				this.myMovie.addGoof(this.goofField.getText());
				loadInfos();
				this.goofField.setText("");
			}
			//Action pour le bouton d'enl�vement de goof
			else if(event.getSource().equals(this.goofRemButton) && !this.goofField.getText().equals(""))
			{
				try
				{
					int testedIndex = new Integer(this.goofField.getText());
					
					if(testedIndex > 0 && testedIndex < this.myMovie.getGoof().size())
					{
						this.myMovie.getGoof().remove(testedIndex);
					}
				}
				catch(Exception e){}
				loadInfos();
				this.goofField.setText("");
			}
			//Action pour le bouton d'ajout de cast
			else if(event.getSource().equals(this.castAddButton) && !this.actorField.getText().equals("") && !this.characterField.getText().equals(""))
			{
				this.myMovie.addCastInfo(this.actorField.getText(), this.characterField.getText());
				loadInfos();
				this.characterField.setText("");
				this.actorField.setText("");				
			}
			//Action pour le bouton d'enl�vement de cast
			else if(event.getSource().equals(this.castRemButton))
			{
				this.myMovie.getActors().remove(this.actorField.getText());
				this.myMovie.getCharacters().remove(this.characterField.getText());
				loadInfos();
				this.characterField.setText("");
				this.actorField.setText("");				
			}
			//Action pour le bouton de lecture
			else if(event.getSource().equals(this.playButton))
			{
				if(Desktop.isDesktopSupported())
				{
					Desktop myDesktop = Desktop.getDesktop();
					if(myDesktop.isSupported(Desktop.Action.OPEN))
					{
						File myFile;
						String fileName = this.myMovie.getAbsolutePath() + this.myMovie.getFileName();
						fileName = fileName.replaceAll(" ", "\\ ");
						
						try
						{							
							myFile = new File(fileName);
							
							myDesktop.open(myFile);
							
							this.seenCheckBox.setSelected(true);
							this.saveButton.doClick();
						}
						catch (Exception e)
						{
							try
							{
								//Ne marchera que chez moi...
								//Impl�menter fonction de recherche du programme, ou permettre a l'utilisateur de rentrer le chemin du programme
								if( System.getProperty("user.name").equals("Kombucha") && System.getProperty("os.name").equals("Windows 7"))
									Runtime.getRuntime().exec("C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe \"" + fileName + "\"");
								else
									throw new IOException();
								
								this.seenCheckBox.setSelected(true);
								this.saveButton.doClick();
							}
							catch (IOException e2)
							{
								try
								{
									myFile = new File(this.myMovie.getAbsolutePath());
									myDesktop.open(myFile);
								}
								catch (Exception e1){}
							}
						}
					}						
				}				
			}
			//Action pour le bouton de changement de fichier
			else if(event.getSource().equals(this.changeFileButton))
			{
				JFileChooser chooseFiles = new JFileChooser();
				
				chooseFiles.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooseFiles.showOpenDialog(this);
				File chosenFile = chooseFiles.getSelectedFile();
				
				if(chosenFile != null)
				{
					this.myMovie.setFileName(chosenFile.getName());
					this.myMovie.setAbsolutePath(chosenFile.getAbsolutePath().substring(0, chosenFile.getAbsolutePath().indexOf(chosenFile.getName())));
					this.myMovie.setSize(new Long(chosenFile.length()).toString());
					
					loadInfos();
				}
			}
			//Action pour le bouton de changement d'informations
			else if(event.getSource().equals(this.changeInfoButton) && this.imdbField.getText().matches("http://www.imdb.com/title/.*?/"))
			{
				this.myFrame.getGlassPane().setVisible(true);
				
				//Lancement du traiment
				Thread myThread = new Thread(new Runnable()
				{
					public void run()
					{
						try
						{
							System.out.println("Searching new info for \"" + myMovie.getFileName() + "\"");
							MovieDataRetriever r = new MovieDataRetriever(imdbField.getText(), false);
							System.out.println("Movie data retriever initialized");
							
							
							myMovie.setTitle(r.getMovieTitle());
							myMovie.setYear(r.getMovieYear());
							myMovie.setDirector(r.getMovieDirector());
							myMovie.setPlot(r.getMoviePlot());
							myMovie.setRate(r.getMovieRate());
							myMovie.setVotes(r.getMovieVotes());
							myMovie.setRuntime(r.getMovieRuntime());
							myMovie.setImdb(r.getImdb());
							myMovie.setRelease("");
							myMovie.setType("");
							
							myMovie.getGenre().removeAll(myMovie.getGenre());
							for(String str : r.getMovieGenres())
								myMovie.addGenre(str);
							
							myMovie.getActors().removeAll(myMovie.getActors());
							myMovie.getCharacters().removeAll(myMovie.getCharacters());
							for(String str : r.getMovieCast())
								myMovie.addCastInfo(str.split("#\\|#")[0], str.split("#\\|#")[1]);
							
							myMovie.getTrivia().removeAll(myMovie.getTrivia());							
							for(String str : r.getMovieTrivia())
								myMovie.addTrivia(str);
							
							myMovie.getGoof().removeAll(myMovie.getGoof());
							for(String str:r.getMovieGoofs())
								myMovie.addGoof(str);
							
							System.out.println("Done!");
						}
						catch(Exception e){e.printStackTrace();};

						
						SwingUtilities.invokeLater(new Runnable()
						{

							public void run()
							{
								myFrame.getGlassPane().setVisible(false);
								loadInfos();
							}
							
						});
					}
				});
				myThread.start();
				
			}
			//Action pour le bouton d'ouverture de lien
			else if(event.getSource().equals(this.imdbOpenButton) && this.imdbField.getText().matches("http://www.imdb.com/title/.*?/"))
			{
				if(Desktop.isDesktopSupported())
				{
					Desktop myDesktop = Desktop.getDesktop();
					if(myDesktop.isSupported(Desktop.Action.BROWSE))
					{
						try
						{
							URI imdbLink = new URI(this.imdbField.getText());
							
							myDesktop.browse(imdbLink);
						}
						catch (IOException e) {e.printStackTrace();}
						catch (URISyntaxException e) {e.printStackTrace();}
					}						
				}	
			}
		}
		
	}

	public void mouseClicked(MouseEvent mc)
	{
		if(mc.getSource() instanceof JTable)
		{
			JTable table = (JTable) mc.getSource();
			if(table.equals(this.castTable))
			{
				Object value = table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
				int index = 0;
				
				String columnName = table.getColumnName(table.getSelectedColumn());
				
				if(columnName.equals("Actor"))
					index = this.myMovie.getActors().indexOf(value);
				else
					index = this.myMovie.getCharacters().indexOf(value);
				
				this.actorField.setText(this.myMovie.getActors().get(index));
				this.characterField.setText(this.myMovie.getCharacters().get(index));
			}
		}
	}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
}
