import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.h2.jdbcx.JdbcConnectionPool;


public class Controller
{
	public final static int LIMIT_TITLE = 100;
	public final static int LIMIT_DIRECTORS = 200;
	public final static int LIMIT_FILENAME = 100;
	public final static int LIMIT_ABSOLUTEPATH = 250;
	public final static int LIMIT_RELEASE = 50;
	public final static int LIMIT_TYPE = 50;
	public final static int LIMIT_IMDB = 50;
	public final static int LIMIT_ACTOR = 150;
	public final static int LIMIT_CHARACTER = 150;
	public final static int LIMIT_GENRE = 50;
	public final static int LIMIT_TRIVIA = 2500;
	public final static int LIMIT_GOOF = 2500;
	public final static int LIMIT_PLOT = 4000;
	
	private JdbcConnectionPool cp;
	private ArrayList<String> movieList;
	private  ArrayList<Movie> failedSearch;
	private Integer countdown;
	private WorkQueue myWorkQueue;
	public boolean workInProgress;
	
	
	
	public Controller()
	{ 
		try
		{
			Class.forName("org.h2.Driver");
		}
		catch (ClassNotFoundException e) {e.printStackTrace();}
				
		this.cp = JdbcConnectionPool.create("jdbc:h2:./datas/movieDB", "root", "root");
		createTables();
		this.movieList = new ArrayList<String>();
		this.failedSearch = new ArrayList<Movie>();
		this.countdown = 0;
		this.workInProgress = false;
	}
	
	public synchronized Connection getConnection()
	{
		try
		{
			synchronized(this.cp)
			{
				return this.cp.getConnection();
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;			
		}
	}
	
	public void closeConnection()
	{
		try
		{
			this.cp.dispose();
		}
		catch (SQLException e) {e.printStackTrace();}
	}
	
	public synchronized void addMovies(String str)
	{
		this.movieList = new ArrayList<String>();
		
		File myFile = new File(str);
		scandir(myFile);
		this.movieList = trimOldMovies(this.movieList);
		if(this.movieList.size() == 0)
			return;
		
		addMoviesQueue();
	}
	
	public synchronized void addMovies(ArrayList<File> list)
	{
		this.movieList = new ArrayList<String>();
		
		for(File f:list)
			scandir(f);
		this.movieList = trimOldMovies(this.movieList);
		if(this.movieList.size() == 0)
			return;
		
		addMoviesQueue();
	}
	
	private synchronized void addMoviesQueue()
	{
		this.failedSearch = new ArrayList<Movie>();
		
		MainMoviesManager.myMainFrame.setProgressBar(this.movieList.size());
		this.countdown = this.movieList.size();
		this.workInProgress = true;
		
		if(this.myWorkQueue == null)
		{
			this.myWorkQueue = new WorkQueue(10);
			System.out.println("Initializing Workqueue");
		}
		
		
		for(final String s : this.movieList)
		{
			myWorkQueue.execute(new Runnable()
			{
				private MovieDataRetriever r = null; 
				private Movie myMovie = null;
				
				public void run()
				{
					try
					{											
						myMovie = new Movie();						
						File myTestFile = new File(s);
						myMovie.setAbsolutePath(myTestFile.getAbsolutePath().substring(0, myTestFile.getAbsolutePath().indexOf(myTestFile.getName())));
						myMovie.setFileName(myTestFile.getName());						
						if(myTestFile.exists())
							myMovie.setSize(new Long(myTestFile.length()).toString());
						myTestFile = null;
						
						r = new MovieDataRetriever(s, true);
						
						myMovie.setTitle(r.getMovieTitle());
						myMovie.setYear(r.getMovieYear());
						myMovie.setDirector(r.getMovieDirector());
						myMovie.setPlot(r.getMoviePlot());
						myMovie.setRate(r.getMovieRate());
						myMovie.setVotes(r.getMovieVotes());
						myMovie.setRuntime(r.getMovieRuntime());
						myMovie.setImdb(r.getImdb());
						
						for(String str : r.getMovieGenres())
							myMovie.addGenre(str);
						
						for(String str : r.getMovieCast())
							myMovie.addCastInfo(str.split("#\\|#")[0], str.split("#\\|#")[1]);
						
						for(String str : r.getMovieTrivia())
							myMovie.addTrivia(str);
						
						for(String str:r.getMovieGoofs())
							myMovie.addGoof(str);
						
						myMovie.saveMovie();							
					}
					catch (Exception e)
					{					
						synchronized(failedSearch)
						{
							failedSearch.add(myMovie);
						}						
					}
					finally
					{
						MainMoviesManager.myMainFrame.updateProgressBar();
						synchronized(countdown)
						{							
							if(--countdown == 0)
							{
								workInProgress = false;
								//Surement inutile...
								//myWorkQueue.finishThreads();
								MainMoviesManager.myMainFrame.updateUIData();
								if(failedSearch.size() > 0)
									SwingUtilities.invokeLater(new Runnable() { public void run() {new FailFrame(failedSearch); }});								
							}								
						}						
						r = null;
						myMovie = null;
					}
					
				}
			});
		}
		
		System.gc();
	}
		
	private ArrayList<String> trimOldMovies(ArrayList<String> list)
	{
		ArrayList<String> temp = list;
		String fullname = "";
		System.out.println("Trimming old movies out");
		try
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			
			ResultSet rs = s.executeQuery("SELECT absolute_path, file_name FROM movie;");
			while(rs.next())
			{
				fullname = rs.getObject("absolute_path").toString() + rs.getObject("file_name").toString();
				temp.remove(fullname);
			}
			
			rs.close();
			s.close();
			c.close();				
		}
		catch (SQLException e) {e.printStackTrace();}
		
		return temp;
	}

	public void removeMovie(int id)
	{
		try
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			
			s.executeUpdate("DELETE FROM movie WHERE id_movie = " + id + ";");
			s.close();
			c.close();				
		}
		catch (SQLException e) {e.printStackTrace();}
		
	}
	
	public void updateSeen(int id, boolean st)
	{
		try
		{
			int state = (st)? 1:0;
			Connection c = getConnection();
			Statement s = c.createStatement();
			
			System.out.println("Seen this movie? " + st);
			s.executeUpdate("UPDATE movie SET seen=" + state + " WHERE id_movie = " + id +";");
			
			MainMoviesManager.myMainFrame.updateUIData();
			s.close();
			c.close();				
		}
		catch (SQLException e) {e.printStackTrace();}
		
	}
	
	
	public void scandir(File f)
	{
		if(f.isDirectory())
		{
			File[] fileList = f.listFiles();
			
			for(File fi : fileList)
				scandir(fi);
		}
		else
		{
			String fileName = f.getName();
			if(fileName.endsWith(".avi")
					|| fileName.endsWith(".mkv")
					|| fileName.endsWith(".iso")
					|| fileName.endsWith(".mpg")
					|| fileName.endsWith(".mp4")
					|| fileName.endsWith(".divx"))
			this.movieList.add(f.getAbsolutePath());
		}			
	}
	
	public void createTables()
	{
		try
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			
	        //SEQUENCE movie
	        s.execute("CREATE SEQUENCE IF NOT EXISTS movie_seq START WITH 1 INCREMENT BY 1;");
	        
	        //Movie table
	        s.execute("CREATE TABLE IF NOT EXISTS movie " +
	       		"(id_movie NUMBER(4,0) CONSTRAINT c_pk_id_movie PRIMARY KEY," +
	       		" title VARCHAR2(" + LIMIT_TITLE + "), " +
	       		" year NUMBER(4,0), " +
	       		" directors VARCHAR2(" + LIMIT_DIRECTORS + "), " +
	       		" plot VARCHAR2 (" + LIMIT_PLOT + "), " + 
	       		" runtime NUMBER(4,0), " +
	       		" rate NUMBER(3,1), " +
	       		" votes NUMBER(6,0), " +
	       		" file_name VARCHAR2(" + LIMIT_FILENAME + "), " +
	       		" absolute_path VARCHAR2(200), " +
	       		" size NUMBER(10,0), " +
	       		" release VARCHAR2(" + LIMIT_RELEASE + "), " +
	       		" type VARCHAR2(" + LIMIT_TYPE + "), " +
	       		" seen NUMBER(1,0)," +
	       		" imdb VARCHAR2(" + LIMIT_IMDB + "));");
	        
	        //Cast table
	        s.execute("CREATE TABLE IF NOT EXISTS cast " +
	        		"(cast_id_movie NUMBER(4,0), " +
	        		" actor VARCHAR2(" + LIMIT_ACTOR + "), " +
	        		" character VARCHAR2(" + LIMIT_CHARACTER + "), " +
	        		" FOREIGN KEY(cast_id_movie) REFERENCES movie(id_movie) ON DELETE CASCADE);");
	        
	        //Genre table
	        s.execute("CREATE TABLE IF NOT EXISTS genre " +
	        		"(genre_id_movie NUMBER(4,0), " +
	        		" genre_name VARCHAR(" + LIMIT_GENRE + "), " +
	        		" FOREIGN KEY(genre_id_movie) REFERENCES movie(id_movie) ON DELETE CASCADE);");
	        
	        //Trivia table
	        s.execute("CREATE TABLE IF NOT EXISTS trivia " +
	        		"(trivia_id_movie NUMBER(4,0), " +
	        		" trivia_txt VARCHAR(" + LIMIT_TRIVIA + "), " +
	        		" FOREIGN KEY(trivia_id_movie) REFERENCES movie(id_movie) ON DELETE CASCADE);");
        
	        //Goofs table
	        s.execute("CREATE TABLE IF NOT EXISTS goofs " +
	        		"(goofs_id_movie NUMBER(4,0), " +
	        		" goof_txt VARCHAR(" + LIMIT_GOOF + "), " +
	        		" FOREIGN KEY(goofs_id_movie) REFERENCES movie(id_movie) ON DELETE CASCADE);");//*/
			
			s.close();
			c.close();
		}
		catch (SQLException e) {e.printStackTrace();}

	}
	
	public void showTable(String table)
	{
        try
        {
        	 int i = 0;
        	Connection c = getConnection();
			Statement s = c.createStatement();
			
	        ResultSet res = s.executeQuery("select * from " + table + ";");
	        ResultSetMetaData resMD = res.getMetaData();
	        
	        System.out.println("==================================");
	        while(res.next())
	        {
	        	
	        	for(i = 1; i <= resMD.getColumnCount(); i++)
	        	{
	        		System.out.print(resMD.getColumnName(i) + " : ");
	        		System.out.println(res.getObject(i).toString());
	        	}
	        	System.out.print("\n");
	        }       
	        
	        s.close();
			c.close();
        }
        catch (SQLException e) {e.printStackTrace();}
	}
	
	public ArrayList<String> getGenres()
	{
		ArrayList<String> genreList = new ArrayList<String>();
		
		try
		{
			int i = 0;
			Connection c = getConnection();
			Statement s = c.createStatement();
			
			ResultSet rs = s.executeQuery("SELECT DISTINCT genre_name FROM genre;");
			ResultSetMetaData rsmd = rs.getMetaData();
			
			while(rs.next())
				for(i = 1; i <= rsmd.getColumnCount(); i++)
					genreList.add(rs.getObject(i).toString());
			
			rs.close();
			s.close();
			c.close();				
		}
		catch (SQLException e) {e.printStackTrace();}		
		
		Collections.sort(genreList);
		
		return genreList;
	}
	
	public String[] getTitles(String table)
	{
		//yep
		String[] titles = null;
		
		try
		{
			Connection c = cp.getConnection();
			
			Statement state = c.createStatement();
			ResultSet res = state.executeQuery("SELECT * FROM " + table + ";");
			ResultSetMetaData resmd = res.getMetaData();
			
			titles = new String[(table.equals("movie"))? (resmd.getColumnCount()+1):(resmd.getColumnCount())];
			for(int i = 0; i < resmd.getColumnCount(); i++)
			{
				titles[i] = resmd.getColumnName(i + 1);
				System.out.println(resmd.getColumnName(i + 1));
			}//*/
			
			if(table.equals("movie"))
				titles[resmd.getColumnCount()] = "MORE";
		
		}
		catch(SQLException sqle){sqle.printStackTrace();}
		
		return titles;
	}
	
	public Object[][] getCast(int id)
	{
		//yep
		Object[][] cast = null;
		
		try
		{
			Connection c = cp.getConnection();
			
			Statement state = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet res = state.executeQuery("SELECT actor, character FROM cast WHERE cast_id_movie = " + id + ";");
			ResultSetMetaData resmd = res.getMetaData();
			
			res.last();
			int rowcount = res.getRow();
			res.beforeFirst();
			
			cast = new Object[rowcount][resmd.getColumnCount()];
	
			while(res.next())
			{
				for(int k = 0; k < resmd.getColumnCount(); k++)
				{
					cast[res.getRow()-1][k] = res.getObject(k+1);
				}
			}	
			
			state.close();
			c.close();			
		}
		catch(SQLException sqle){sqle.printStackTrace();}
		
		return cast;
	}

	private String createQuery(String table, String[] fields, ArrayList<String> conditions)
	{
		StringBuffer query = new StringBuffer();
		query.append("SELECT ");
		
		int i = 0;
		for(i = 0; i < fields.length - 1; i++)
			query.append(fields[i] + ", ");
		
		query.append(fields[fields.length - 1]);
		query.append(" FROM ").append(table);
		
		
		if(conditions == null || conditions.size() == 0)
			query.append(";");
		else if(conditions.size() == 1)
			query.append(" WHERE " + conditions.get(0)).append(";");
		else
		{
			query.append(" WHERE ");
			query.append(conditions.get(0));
			
			for(i = 1; i < conditions.size(); i++)
			{
				query.append(" AND ");
				query.append(conditions.get(i));
			}
			
			query.append(";");
		}
		
		return query.toString();
	}
	
	public Object[][] getMainTableDatas(ArrayList<String> conditions, boolean trim)
	{
		Object[][] datas = null;
		
		try
		{
			Connection c = cp.getConnection();
			
			Statement state = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			String[] fields = {"id_movie", "title", "year", "runtime", "rate", "seen", "absolute_path || file_name"};
			String query = createQuery("movie", fields, conditions);
			query = query.substring(0, query.length()-1) + " ORDER BY id_movie;";
			System.out.println(query.toString());
			ResultSet res = state.executeQuery(query);
			ResultSetMetaData resmd = res.getMetaData();
			
			res.last();
			int rowcount = res.getRow();
			res.beforeFirst();
			
			//count existing movies
			ArrayList<Integer> availableMovies = new ArrayList<Integer>();
			if(trim)
			{
				
				while(res.next())
					if(new File(res.getString(resmd.getColumnCount())).exists())
						availableMovies.add(res.getRow());
				System.out.println(availableMovies.size() + " files can be accessed");
				res.beforeFirst();
				
				rowcount = availableMovies.size();
			}				
			
			datas = new Object[rowcount][resmd.getColumnCount() + 2];
			
			int filePathIndex = resmd.getColumnCount() - 1;
			int moreIndex = resmd.getColumnCount();
			int removeIndex = resmd.getColumnCount() + 1;
			
			int row = -1;
	
			while(res.next())
			{
				//S'il faut enlever les films inaccessibles, on saute cette boucle
				if(trim & !availableMovies.contains(res.getRow()))
					continue;				
				row++;
				
				for(int k = 0; k <= resmd.getColumnCount(); k++)
				{
					if(k == moreIndex)
					{
						JButton myButton = new JButton("more");
						myButton.setName((String) datas[row][1]);
						datas[row][k] = myButton;
					}
					else if(k == removeIndex)
					{
						JButton myButton = new JButton("remove");
						//myButton.setName((String) datas[row][1]);
						datas[row][k] = myButton;
					}
					else if(k != filePathIndex)
					{						
						if(resmd.getColumnName(k+1).equals("SEEN"))
							if(res.getObject(k+1).toString().equals("0"))
								datas[row][k] = false;
							else
								datas[row][k] = true;
						else
							datas[row][k] = res.getObject(k+1);
					}
				}
			}	
			
			state.close();
			c.close();			
		}
		catch(SQLException sqle){sqle.printStackTrace();}

		return datas;
	}
	
}
